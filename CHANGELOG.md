# Generalized Arrays

## 2.1.0 - 2024-11-18

### Changed

-   Argument / type checks are now done using SRFI-253 instead of the `check-errors` egg. This makes
    the egg fully portable across different R7RS implementations.

## 2.0.2 - 2024-04-22

### Added

-   Added SRFI-143 v1.0.0 back in over `(chicken fixnum)`. The problems with that egg have now been
    resolved, and as of version 1.0.0 performance (and the function signatures) are basically
    re-exports of `(chicken fixnum)` now. This is done to make it easier to port the R7RS egg
    without having to make more edits to the code.

## 2.0.1 - 2024-04-22

### Fixed

-   Import reference in `tests/run.scm` so that the appropriate tests are imported.

## 2.0.0 - 2024-02-17

### Changed

-   The egg and its imports were changed back to being prefixed as "generalized-arrays" instead of
    "arrays". This is because there already exists an egg which provides
    [`(import arrays)`](https://api.call-cc.org/5/doc/arrays). No other functionality should have
    changed though.

## 1.0.1 - 2024-02-03

### Fixed

-   A stray reference to SRFI-143 was present in the `(arrays storage)` module, which caused
    salmonella to break since SRFI-143 was removed in favour of `(chicken fixnum)` in the .egg file.

## v1.0.0 - 2024-02-03

### Added

-   Initial release with revamped API. This was effectively a rewrite of the entire library, and so
    it should be treated as a pretty hard break from what was 0.0.7.

### Removed

-   Many APIs from 0.0.7 were removed as they are more easily expressed via the `transducers`
    library.

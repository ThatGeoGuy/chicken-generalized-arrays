# Dockerfile for generalized-arrays
#
# Instructions:
#
#    docker build \
#        -f Dockerfile \
#        -t registry.gitlab.com/thatgeoguy/chicken-generalized-arrays:latest \
#        -t registry.gitlab.com/thatgeoguy/chicken-generalized-arrays:VERSION .
#
#    docker push registry.gitlab.com/thatgeoguy/chicken-generalized-arrays:latest
#    docker push registry.gitlab.com/thatgeoguy/chicken-generalized-arrays:VERSION

FROM alpine:latest AS builder

RUN set -eux; \
    # Install chicken \
    apk add make mold chicken; \
    # \
    # Install beaker (chicken-clean, chicken-lint) \
    chicken-install beaker; \
    # \
    # Dependencies for generalized-arrays \
    chicken-install \
    r7rs \
    srfi-48 \
    srfi-128 \
    srfi-133 \
    srfi-143:1.0.0 \
    srfi-160 \
    srfi-253 \
    transducers:0.5.1; \
    # \
    # Test dependencies for arrays \
    chicken-install test;

LABEL org.opencontainers.image.authors="Jeremy Steward <jeremy@thatgeoguy.ca>"
LABEL org.opencontainers.image.version="0.4.0"
LABEL org.opencontainers.image.licenses="BSD-3"
LABEL org.opencontainers.image.title="generalized-arrays"
LABEL org.opencontainers.image.description="Docker image for generalized-arrays project"

CMD ["sh"]

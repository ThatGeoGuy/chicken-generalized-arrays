;;; Copyright (c) 2023 Jeremy Steward
;;; All rights reserved.
;;;
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are met:
;;;
;;; 1. Redistributions of source code must retain the above copyright notice,
;;; this list of conditions and the following disclaimer.
;;;
;;; 2. Redistributions in binary form must reproduce the above copyright notice,
;;; this list of conditions and the following disclaimer in the documentation
;;; and/or other materials provided with the distribution.
;;;
;;; 3. Neither the name of the copyright holder nor the names of its
;;; contributors may be used to endorse or promote products derived from this
;;; software without specific prior written permission.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;; CONSEQUENTIAL DAMAGES  INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.

(export
  ;; Record type
  <array>
  ;; Predicates
  array?
  ;; Metadata accessors
  array-storage-class
  array-interval
  array-storage-object
  array-rank
  array-shape
  array-stride
  array-ref
  array-set!
  array-update!
  array=?
  ;; Constructors
  make-array
  make-array-from-storage
  array-tabulate
  array-broadcast
  ;; Transductions
  array-fold
  array-interval-fold
  reverse-array-fold
  reverse-array-interval-fold
  flatten-array
  chain-array
  interleave-array
  zip-array
  reverse-flatten-array
  reverse-chain-array
  reverse-interleave-array
  reverse-zip-array
  collect-array
  array-transducible
  reverse-array-transducible
  ;; Views
  array-view?
  array-slice
  array-transpose
  array-swap-axes
  array-squeeze-axis
  array-squeeze
  array-expand-axis
  ;; Copying and Conversion
  array-copy
  array-copy!
  array-append
  array-reshape
  array-reclassify
  ;; IO
  array->nested-list
  array-read
  array-write)

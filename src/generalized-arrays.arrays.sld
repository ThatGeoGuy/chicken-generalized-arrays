;;; Copyright (c) 2023 Jeremy Steward
;;; All rights reserved.
;;;
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are met:
;;;
;;; 1. Redistributions of source code must retain the above copyright notice,
;;; this list of conditions and the following disclaimer.
;;;
;;; 2. Redistributions in binary form must reproduce the above copyright notice,
;;; this list of conditions and the following disclaimer in the documentation
;;; and/or other materials provided with the distribution.
;;;
;;; 3. Neither the name of the copyright holder nor the names of its
;;; contributors may be used to endorse or promote products derived from this
;;; software without specific prior written permission.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;; CONSEQUENTIAL DAMAGES  INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.

(cond-expand
  (chicken-5
    (declare (safe-globals) (specialize))
    (declare (fixnum-arithmetic))))

(define-library (generalized-arrays arrays)
  (import (scheme base)
          (scheme case-lambda)
          (scheme read)
          (srfi 48)
          (srfi 128)
          (srfi 133)
          (srfi 143)
          (generalized-arrays intervals)
          (generalized-arrays storage)
          (transducers base)
          (transducers lists)
          (transducers vectors)
          (transducers numbers))

  (cond-expand
    (chicken-5
      (import (only (chicken base) compose))
      (import-syntax (srfi 253))
      (import-syntax (chicken type)))
    (else
      (define compose values)))

  (include-library-declarations "src/generalized-arrays.arrays.exports.scm")

  (begin
    ;; Predicate to check for a non-negative (zero or greater) fixnum
    (define (non-negative-fixnum? x)
      (and (fixnum? x) (fx>=? x 0))))

  (include "src/arrays/record.scm"
           "src/arrays/transducible.scm"
           "src/arrays/view.scm"
           "src/arrays/convert.scm"
           "src/arrays/io.scm")
  ;; End-of-module
  )

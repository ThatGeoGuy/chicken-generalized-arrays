;;; Copyright (c) 2023 Jeremy Steward
;;; All rights reserved.
;;;
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are met:
;;;
;;; 1. Redistributions of source code must retain the above copyright notice,
;;; this list of conditions and the following disclaimer.
;;;
;;; 2. Redistributions in binary form must reproduce the above copyright notice,
;;; this list of conditions and the following disclaimer in the documentation
;;; and/or other materials provided with the distribution.
;;;
;;; 3. Neither the name of the copyright holder nor the names of its
;;; contributors may be used to endorse or promote products derived from this
;;; software without specific prior written permission.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
;;; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;; CONSEQUENTIAL DAMAGES  INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;; POSSIBILITY OF SUCH DAMAGE.

(export
  <storage-class>
  ;; Constructors
  make-storage-class
  ;; Predicates
  storage-class?
  ;; Accessors
  storage-class-short-id
  storage-class-constructor
  storage-class-ref
  storage-class-set
  storage-class-length
  storage-class-copy
  storage-class-copy!
  storage-class-transducible
  storage-class-comparator
  storage-class-default-element
  ;; Invokers
  make-storage-object
  storage-object-ref
  storage-object-set!
  storage-object-length
  storage-object-copy
  storage-object-copy!
  ;; Standard storage classes
  vector-storage-class
  u8vector-storage-class
  s8vector-storage-class
  u16vector-storage-class
  s16vector-storage-class
  u32vector-storage-class
  s32vector-storage-class
  u64vector-storage-class
  s64vector-storage-class
  f32vector-storage-class
  f64vector-storage-class
  c64vector-storage-class
  c128vector-storage-class)
